# Dialog JS

Egyszerű dialogot megvalósító tool natív JS-hez.

## Telepítés
`npm install @icom/dialog`

## Használat
```
const deleteDialog = new Dialog({
  title: 'Biztos, hogy törölni szeretnéd?',
  content: 'Ez a művelet nem visszavonható',
  buttonPrimary: 'Igen, törlöm',
  buttonSecondary: 'Mégsem'
});


deleteDialog.open();
```

## Opciók

#### id
- type: `String`
- default: generált UID

A dialog parent elemének `id` attribútuma.

#### title
- type: `String`
- default: `''`

A dialog címe.

#### title
- type: `String`
- default: `''`

A dialog tartalma. Opcionálisan megadható egy DOM elem is, ilyenkor az eredeti elemet eltávolítja a DOM-ból és klónozza a Dialogba.

#### closeButtonTitle
- type: `String`
- default: `Bezárás`

A bezárás gomb eszköztippje.

#### buttonPrimary
- type: `String`
- default: `Rendben`

Az elsődleges gomb szövege. Ha üres stringet adunk át, a gomb nem jelenik meg.

#### buttonSecondary
- type: `String`
- default: `''`

A másodlagos gomb szövege. Ha üres stringet adunk át, a gomb nem jelenik meg.

#### autoOpen
- type: `Boolean`
- default: `false`

A dialog pédányosításkor megjelenjen-e.

#### disableScroll
- type: `Boolean`
- default: `true`

Eldönti, hogy letiltsuk-e a body görgetését amíg nyitva van a dialog.

#### removeScrollbar
- type: `Boolean`
- default: `true`

Eldönti, hogy eltávolítsuk-e a scrollbart, ha a görgetés le van tiltva.

#### destroyOnClose
- type: `Boolean`
- default: `false`

Eldönti, hogy eltávolítsuk-e a DOM-ból a dialogot a bezárás után. Ha az `autoOpen`-t true-ra állítjuk, hasznos opció lehet.

#### closeOnPrimary
- type: `Boolean`
- default: `true`

Eldönti, hogy bezárjuk-e a dialogot az elsődleges gomb megnyomásakor.

#### closeOnSecondary
- type: `Boolean`
- default: `true`

Eldönti, hogy bezárjuk-e a dialogot az másodlagos gomb megnyomásakor.

#### closeOnEsc
- type: `Boolean`
- default: `true`

Eldönti, hogy bezárjuk-e a dialogot az `Esc` gomb megnyomásakor.

#### closeOnBackdropClick
- type: `Boolean`
- default: `true`

Eldönti, hogy bezárjuk-e a dialogot a backdropra való klikkeléskor.

#### callbackPrimary
- type: `Function`
- default: `() => {}`

Ez a callback metódus fog lefutni az elsődleges gombra kattintáskor.

#### callbackSecondary
- type: `Function`
- default: `() => {}`

Ez a callback metódus fog lefutni az másodlagos gombra kattintáskor.

#### callbackClose
- type: `Function`
- default: `() => {}`

Ez a callback metódus fog lefutni a dialog bezárásakor.

#### afterHide
- type: `Function`
- default: `() => {}`

Ez a callback metódus fog lefutni a dialog bezárása után a transition végeztével.

#### container
- type: `HTMLElement`
- default: `document.body`

Ide fogja beszúrni a DOM-ba a dialog elemet.

#### classNames
- type: `Object`
- default:
```javascript
{
  base: ['dialog'],
  box: ['dialog__box'],
  title: ['dialog__title'],
  content: ['dialog__content'],
  actions: ['dialog__actions'],
  action: ['dialog__action'],
  close: ['dialog__close'],
  primaryButton: ['dialog__button', 'button--primary'],
  secondaryButton: ['dialog__button', 'button--secondary'],
  visibleState: ['is-opened'],
  containerVisibleState: ['is-dialog-opened'],
}
```

Ezekből a classokból épül fel a dialog DOM stuktúrája.

## Fejlesztés

Telepítés: `npm install`
Demo fájl megnyitása: `npm run dev`
Lintelés: `npm run lint`

Új verzió kiadása: `npm run release [major|minor|patch]`
