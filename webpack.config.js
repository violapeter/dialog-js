module.exports = (env, argv) => ({
  entry: './index.js',
  output: {
    filename: 'main.min.js',
    library: 'Dialog',
    libraryTarget: 'var',
    libraryExport: 'default',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [[
              '@babel/preset-env', { useBuiltIns: 'entry' },
            ]],
          },
        },
      },
    ],
  },
  stats: argv.mode === 'production' ? 'normal' : 'minimal',
});
