/* global document, window, HTMLElement */

import '@babel/polyfill';
import ScrollLock from '@icom/scroll-lock';

const locker = new ScrollLock();

const setClasses = (domElement, classNames) => {
  if (!domElement) {
    return;
  }

  classNames.forEach((className) => {
    domElement.classList.add(className);
  });
};
const unsetClasses = (domElement, classNames) => {
  classNames.forEach((className) => {
    domElement.classList.remove(className);
  });
};

const getContentFromDOM = (content) => {
  if (content.parentNode) {
    const cloned = content.cloneNode(true);
    content.parentNode.removeChild(content);
    return cloned;
  }
  return content;
};

const getUID = () => `dialog-${(+new Date()).toString(36)}`;

const getDefaultClassNames = () => ({
  base: ['dialog'],
  box: ['dialog__box'],
  title: ['dialog__title'],
  content: ['dialog__content'],
  actions: ['dialog__actions'],
  action: ['dialog__action'],
  close: ['dialog__close'],
  primaryButton: ['dialog__button', 'button--primary'],
  secondaryButton: ['dialog__button', 'button--secondary'],
  visibleState: ['is-opened'],
  containerVisibleState: ['is-dialog-opened'],
});

const DEFAULT_OPTIONS = {
  id: getUID(),
  title: '',
  content: '',
  closeButtonTitle: 'Bezárás',
  buttonPrimary: 'Rendben',
  buttonSecondary: '',
  autoOpen: false,
  disableScroll: true,
  removeScrollbar: true,
  destroyOnClose: false,
  closeOnPrimary: true,
  closeOnSecondary: true,
  closeOnEsc: true,
  closeOnBackdropClick: false,
  callbackPrimary: () => {},
  callbackSecondary: () => {},
  callbackClose: () => {},
  afterHide: () => {},
  container: document.body,
  classNames: getDefaultClassNames(),
};

class Dialog {
  constructor(options = DEFAULT_OPTIONS) {
    this.options = Object.assign({}, DEFAULT_OPTIONS, options);
    this.dom = null;
    this.opened = this.options.autoOpen;

    this.render();
  }

  open() {
    if (locker.isScrollEnabled()) {
      locker.disable(true);
    }
    setClasses(this.dom.dialog, this.options.classNames.visibleState);
    setClasses(this.options.container, this.options.classNames.containerVisibleState);
    this.opened = true;
  }

  close() {
    this.setAfterTransitionEvents(() => {
      if (this.options.disableScroll && !locker.isScrollEnabled()) {
        locker.enable();
      }
      this.options.afterHide();
      if (this.options.destroyOnClose) {
        this.destroy();
      }
    });
    unsetClasses(this.dom.dialog, this.options.classNames.visibleState);
    unsetClasses(this.options.container, this.options.classNames.containerVisibleState);
    this.opened = false;
  }

  setAfterTransitionEvents(callback) {
    let transitionAlreadyEnds = false;
    const transitionEvents = [
      'webkitTransitionEnd',
      'otransitionend',
      'oTransitionEnd',
      'msTransitionEnd',
      'transitionend',
    ];
    transitionEvents.forEach((transitionEvent) => {
      this.dom.dialog.addEventListener(transitionEvent, () => {
        if (transitionAlreadyEnds) return;
        if (typeof callback === 'function') {
          callback();
        }
        transitionAlreadyEnds = true;
      }, {
        once: true,
      });
    });
  }

  render() {
    this.destroy();
    this.buildDialogElement();
    this.appendDialog();
    this.addEventListeners();
    this.addEscapeEventListener();
    this.addBackdropEventListener();

    if (!this.opened && this.options.autoOpen) {
      setTimeout(this.open, 0);
    }
  }

  appendDialog() {
    this.options.container.appendChild(this.dom.dialog);
  }

  addEventListeners() {
    if (typeof this.options.callbackPrimary === 'function' && this.options.buttonPrimary) {
      this.dom.buttonPrimary.addEventListener('click', () => {
        this.options.callbackPrimary(this);
        if (this.options.closeOnPrimary) this.close();
      });
    }
    if (typeof this.options.callbackPrimary === 'function' && this.options.buttonSecondary) {
      this.dom.buttonSecondary.addEventListener('click', () => {
        this.options.callbackSecondary(this);
        if (this.options.closeOnSecondary) this.close();
      });
    }
    this.dom.close.addEventListener('click', () => {
      if (typeof this.options.callbackClose === 'function') {
        this.options.callbackClose(this);
      }
      this.close();
    });
  }

  addBackdropEventListener() {
    if (!this.options.closeOnBackdropClick) return;

    this.dom.dialog.addEventListener('click', (e) => {
      if (e.target === e.currentTarget) {
        this.close();
      }
    });
  }

  addEscapeEventListener() {
    const KEYCODE_ESCAPE = 27;

    if (!this.options.closeOnEsc) return;

    const afterCloseEscape = (e) => {
      if (e.keyCode === KEYCODE_ESCAPE) {
        this.close();
        window.removeEventListener('keydown', afterCloseEscape);
      }
    };

    window.addEventListener('keydown', afterCloseEscape);
  }

  destroy() {
    if (!this.dom) return;
    this.dom.remove();
  }

  buildDialogElement() {
    let primaryButtonElement = null;
    let secondaryButtonElement = null;

    // dialogElement
    const dialogElement = document.createElement('div');
    setClasses(dialogElement, this.options.classNames.base);
    if (this.opened) { setClasses(dialogElement, this.options.classNames.visibleState); }
    dialogElement.setAttribute('tabindex', '-1');
    dialogElement.setAttribute('role', 'dialog');
    dialogElement.setAttribute('aria-describedby', 'dialog');
    dialogElement.setAttribute('aria-labelledby', this.options.id);
    dialogElement.dataset.id = this.options.id;
    const boxElement = document.createElement('div');
    setClasses(boxElement, this.options.classNames.box);
    dialogElement.appendChild(boxElement);
    // close button
    const SVG_NAMESPACE = 'http://www.w3.org/2000/svg';
    const CLOSE_RAW_SVG = 'M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 '
      + '5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z';
    const closeIcon = document.createElementNS(SVG_NAMESPACE, 'path');
    const closeElement = document.createElementNS(SVG_NAMESPACE, 'svg');
    closeIcon.setAttribute('d', CLOSE_RAW_SVG);
    closeElement.setAttribute('viewBox', '0 0 24 24');
    closeElement.setAttribute('class', this.options.classNames.close);
    closeElement.appendChild(closeIcon);
    closeElement.setAttribute('title', this.options.closeButtonTitle);
    boxElement.appendChild(closeElement);
    // title
    if (this.options.title) {
      const titleElement = document.createElement('div');
      setClasses(titleElement, this.options.classNames.title);
      titleElement.innerHTML = this.options.title;
      boxElement.appendChild(titleElement);
    }
    // content
    if (this.options.content) {
      const contentElement = document.createElement('div');
      setClasses(contentElement, this.options.classNames.content);
      if (this.options.content instanceof HTMLElement) {
        contentElement.appendChild(getContentFromDOM(this.options.content));
      } else {
        contentElement.innerHTML = this.options.content;
      }
      boxElement.appendChild(contentElement);
    }
    // buttons
    if (this.options.buttonPrimary) {
      primaryButtonElement = document.createElement('button');
      setClasses(
        primaryButtonElement,
        this.options.classNames.primaryButton,
        this.options.classNames.action,
      );
      primaryButtonElement.innerHTML = this.options.buttonPrimary;
    }
    if (this.options.buttonSecondary) {
      secondaryButtonElement = document.createElement('button');
      setClasses(
        secondaryButtonElement,
        this.options.classNames.secondaryButton,
        this.options.classNames.action,
      );
      secondaryButtonElement.innerHTML = this.options.buttonSecondary;
    }
    // buttons wrapper
    if (this.options.buttonSecondary || this.options.buttonPrimary) {
      const actionsElement = document.createElement('div');
      setClasses(actionsElement, this.options.classNames.actions);
      if (this.options.buttonSecondary) actionsElement.appendChild(secondaryButtonElement);
      if (this.options.buttonPrimary) actionsElement.appendChild(primaryButtonElement);
      boxElement.appendChild(actionsElement);
    }
    this.dom = {
      dialog: dialogElement,
      buttonPrimary: primaryButtonElement,
      buttonSecondary: secondaryButtonElement,
      close: closeElement,
    };
  }
}

export default Dialog;
